# TOMMAN

## Software yang Diperlukan
Pengembangan TOMMANv2 menggunakan *Virtual Machine* via *Laravel Homestead*, 
dan untuk *deployment* disarankan menggunakan *cloud computing* via *Laravel Forge*

### VirtualBox
Silahkan *download* dan *install* **VirtualBox** melalui https://www.virtualbox.org/wiki/Downloads

Pastikan memilih versi VirtualBox yang *compatible* dengan versi Vagrant yang akan diinstall. 
Kompatibilitas bisa dilihat di halaman
https://www.vagrantup.com/docs/virtualbox/

### Vagrant
*Download* dan *install* **Vagrant** melalui
https://www.vagrantup.com/downloads.html

### Git
Untuk Linux, `git` bisa di-*install* melalui *package manager*. 

Untuk Mac OSX, silahkan install `XCode` dan *additional required components*.

Untuk Windows bisa di-*download* melalui https://git-scm.com/download/win 

### PHP dan Composer
Instalasi `PHP` bergantung pada sistem operasi yang digunakan. 
Silahkan gunakan metode instalasi yang anda sukai. Pastikan `php` bisa diakses
melalui *command line* dengan memasukkannya ke dalam *environment variable* `PATH`

*Composer* bisa di-*download* melalui https://getcomposer.org/download/
 

## Instalasi TOMMAN

### 1. Clone dari git
Untuk berkontribusi, silahkan *fork* dari repository utama di 
https://bitbucket.org/telkomakses/tomman

Untuk instalasi, silahkan *clone* dari repository utama
```
git clone https://meongx@bitbucket.org/telkomakses/tomman.git tomman
```

### 2. Install Libraries via Composer
```
composer install
```

### 3. Init Homestead
Windows
```
vendor\bin\homestead make
```

Linux/OSX/Cygwin
```
./vendor/bin/homestead make
```

### 4. SSH Keypair
Instalasi default vagrant memerlukan *keypair* untuk koneksi ssh.

Pada OSX/Linux/Cygwin, *keypair* bisa dibuat dengan

    ssh-keygen <email>

Untuk Windows, bisa menggunakan `PuTTYgen`

Apabila tidak ingin menggunakan *keypair*, hapus konfigurasi yg berkaitan
pada `homestead.yaml` bagian `authorize` dan `keys`. 

## Run
- Nyalakan *server*

        vagrant up

    Saat pertama kali melakukan `vagrant up`, aplikasi akan melakukan *download*, *install*, dan konfigurasi 
    beberapa aplikasi tambahan, termasuk mengacak *password* untuk *database*.
    Hal ini akan memakan waktu beberapa menit, tergantung kecepatan internet

- Buka browser ke halaman 

    http://localhost:8000

    atau

    https://localhost:44300

## Koneksi SSH
Apabila memiliki *keypair*, cukup lakukan

    vagrant ssh
    
Jika tidak, lakukan koneksi ssh ke `localhost` pada port `2222`, 
dengan username `vagrant` dan password `vagrant` 
