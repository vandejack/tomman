const px = 'resources/assets/vendor/pixeladmin/js/build';
const lib = 'resources/assets/vendor/pixeladmin/libs';

const scripts = [
  /*
   * jQuery
   */
  `${lib}/jquery/dist/jquery.js`,

  /*
   * VueJS
   */
  'node_modules/vue/dist/vue.js',

  /*
   * Google Maps
   */
  'node_modules/@google/markerclusterer/src/markerclusterer.js',

  /*
   * Bootstrap
   */
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/transition.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/button.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/tab.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/collapse.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/alert.js`,
  `${lib}/bootstrap-sass/assets/javascripts/bootstrap/modal.js`,
  // `${lib}/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js`,
  // `${lib}/bootstrap-sass/assets/javascripts/bootstrap/popover.js`,
  // `${lib}/bootstrap-sass/assets/javascripts/bootstrap/carousel.js`,
  // `${lib}/bootstrap-sass/assets/javascripts/bootstrap/affix.js`,
  // `${lib}/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js`,

  /*
   * PixelAdmin
   */
  `${px}/polyfills.js`,
  `${px}/util.js`,
  `${px}/pixeladmin.js`,
  // `${px}/extensions/modal.js`,
  // `${px}/extensions/tooltip.js`,
  // `${px}/extensions/popover.js`,
  // `${px}/plugins/px-file.js`,
  `${lib}/perfect-scrollbar/js/perfect-scrollbar.jquery.js`,
  `${px}/extensions/perfect-scrollbar.jquery.js`,
  `${px}/plugins/px-sidebar.js`,
  `${px}/plugins/px-navbar.js`,
  `${px}/plugins/px-nav.js`,
  `${px}/plugins/px-footer.js`,
  // `${px}/plugins/px-wizard.js`,
  // `${lib}/markdown/lib/markdown.js`,
  // `${lib}/moment/moment.js`,
  // `${px}/plugins/px-block-alert.js`,
  // `${px}/plugins/px-tab-resize.js`,
  // `${px}/plugins/px-expanding-input.js`,
  `${lib}/jquery-validation/dist/jquery.validate.js`,
  `${lib}/jquery-validation/dist/additional-methods.js`,
  `${px}/plugins/px-validate.js`,
  // `${px}/plugins/px-responsive-bg.js`,
  // `${px}/plugins/px-char-limit.js`,
  `${lib}/select2/dist/js/select2.full.js`,
  // `${lib}/bootstrap-datepicker/dist/js/bootstrap-datepicker.js`,
  // `${px}/extensions/bootstrap-datepicker.js`,
  // `${lib}/bootstrap-timepicker/js/bootstrap-timepicker.js`,
  // `${px}/extensions/bootstrap-timepicker.js`,
  // `${lib}/jquery-minicolors/jquery.minicolors.js`,
  // `${lib}/jquery.maskedinput/dist/jquery.maskedinput.js`,
  // `${lib}/jquery-autosize/jquery.autosize.js`,
  // `${lib}/bootbox/bootbox.js`,
  // `${lib}/datatables/media/js/jquery.dataTables.js`,
  // `${lib}/datatables/media/js/dataTables.bootstrap.js`,
  // `${px}/extensions/datatables.js`,
  // `${lib}/growl/javascripts/jquery.growl.js`,
  // `${px}/extensions/growl.js`,
  // `${lib}/dropzone/dist/dropzone.js`,
  // `${px}/extensions/dropzone.js`,
  // `${lib}/jquery-knob/js/jquery.knob.js`,
  // `${px}/extensions/jquery.knob.js`,
  // `${lib}/summernote/dist/summernote.js`,
  // `${px}/extensions/summernote.js`,
  // `${lib}/bootstrap-markdown/js/bootstrap-markdown.js`,
  // `${px}/extensions/bootstrap-markdown.js`,
  // `${lib}/ionrangeslider/js/ion.rangeSlider.js`,
  // `${lib}/jquery-sortable/source/js/jquery-sortable.js`,
  // `${lib}/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js`,
  // `${lib}/quick-select/dist/js/jquery.quickselect.js`,
  // `${lib}/editable-table/mindmup-editabletable.js`,
  // `${lib}/toastr/toastr.js`,
  // `${lib}/bootstrap-daterangepicker/daterangepicker.js`,
  // `${lib}/bootstrap3-typeahead/bootstrap3-typeahead.js`,
  // `${lib}/bootstrap-tagsinput/dist/bootstrap-tagsinput.js`,
  // `${px}/extensions/bootstrap-tagsinput.js`,
  `${lib}/sortablejs/Sortable.js`,
  // `${lib}/seiyria-bootstrap-slider/dist/bootstrap-slider.js`,.
  // `${lib}/nouislider/distribute/nouislider.js`,
  // `${lib}/owl.carousel/dist/owl.carousel.js`,
  // `${lib}/raphael/raphael.js`,
  // `${lib}/d3/d3.js`,
  // `${lib}/morris.js/morris.js`,
  // `${lib}/flot/jquery.flot.js`,
  // `${lib}/flot/jquery.flot.pie.js`,
  // `${lib}/flot/jquery.flot.categories.js`,
  // `${lib}/flot/jquery.flot.resize.js`,
  // `${lib}/flot.tooltip/js/jquery.flot.tooltip.js`,
  // `${lib}/chart.js/dist/Chart.js`,
  // `${lib}/chartist/dist/chartist.js`,
  // `${lib}/c3/c3.js`,
  // `${lib}/jquery.sparkline.dist/dist/jquery.sparkline.js`,
  // `${px}/plugins/px-sparkline.js`,
  // `${lib}/easy-pie-chart/dist/jquery.easypiechart.js`,
];

module.exports = scripts;
