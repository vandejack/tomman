@extends('app')

@section('body')
  <div class="page-header">
    <h1>
      <span class="label label-info">WZ</span>
      <a href="/mcore/sto/workzone/{{ $stoData->workzone_id }}">{{ $stoData->workzone_label }}</a>
      <i class="separator ion-chevron-right"></i>
      <span class="label label-info">STO</span>
      <a href="/mcore/sto/{{ $stoData->id }}">{{ $stoData->label }}</a>
    </h1>
  </div>

  <div class="widget-timeline">
    @foreach($historyList as $history)
      <?php
        $datetime = new \DateTime($history->timestamp);
        $datetime->setTimezone($timezone);

        switch($history->operation) {
          case 'insert':
            $bgClass = 'bg-primary';
            $opText = 'CREATE';
            break;

          case 'update':
            $bgClass = 'bg-info';
            $opText = 'UPDATE';
            break;

          default:
            $bgClass = '';
            $opText = $history->operation;
            break;
        }
      ?>
      <div class="widget-timeline-item">
        <div class="widget-timeline-info">
          <div class="widget-timeline-bullet {{ $bgClass }}"></div>
          <div class="widget-timeline-time {{ $bgClass }}">{{ $datetime->format('G:i') }}</div>
        </div>

        <div class="panel">
          <div class="panel-title">[{{ $history->user_login }}] {{ $history->user_label }}</div>
          <div class="panel-body">{{ $opText }}</div>
          <div class="panel-footer"><em>{{ $datetime->format('d F Y, h:i:s a') }}</em></div>
        </div>
      </div>
    @endforeach
  </div>
@endsection
