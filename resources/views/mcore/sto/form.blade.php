@extends('app')

<?php if (!isset($stoData)) $stoData = null ?>
<?php $title = $stoData->label ?? 'Input STO' ?>
@section('title', $title)

@section('style')
  <style>
    a.list-group-item {
      font-size: larger;
    }
  </style>
@endsection

@section('body')
  @if (isset($stoData->id))
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/sto/workzone/{{ $stoData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $stoData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">STO</span>
        <span>{{ $stoData->label }}</span>
      </li>
    </ol>
  @endif

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  <div class="row">
    <div class="col-md-6 {{ isset($stoData) ? '' : 'col-md-push-3' }}">
      <form id="sto-form" class="panel" method="post">
        {{ csrf_field() }}

        <div class="panel-body">
          @include('partial.form.text', [
            'label' => 'Nama STO',
            'object' => $stoData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Nama STO'
            ]
          ])

          @include('partial.workzone.formcontrol', [
            'label' => 'Work Zone',
            'object' => $stoData,
            'field' => 'workzone_id',
            'displayField' => 'workzone_label',
            'canEdit' => $canEdit,
            'workzoneTree' => $workzoneTree,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan pilih Area Kerja'
            ]
          ])

          @include('partial.mapmarker.formcontrol', [
            'label' => 'Koordinat (lat, lng)',
            'object' => $stoData,
            'field' => 'coordinate',
            'canEdit' => $canEdit
          ])
        </div>

        @if ($canEdit)
          <div class="panel-footer text-right">
            <button class="btn btn-primary">
              <i class="fas fa-check"></i>
              <span>Simpan</span>
            </button>
          </div>
        @endif
      </form>

      @if (isset($stoData))
        <div class="text-right font-size-11 m-b-4">
          <a href="/mcore/sto/{{ $stoData->id }}/history">
            <?php $op = $stoData->last_operation ?>
            @if ($op->operation == 'insert')
              Created
            @else
              Updated
            @endif

            by {{ $op->user_nama }}
            {{ $op->datetime->format('d/m/Y G:i') }}
          </a>
        </div>
      @endif
    </div>

    @if (isset($stoData))
      <div class="col-md-6">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Ruangan</h4>
          </div>

          <div class="list-group">
            @foreach($roomList as $room)
              <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $room->id }}" class="list-group-item">{{ $room->label }}</a>
            @endforeach
          </div>

          @if ($canEdit)
            <form id="room-form" action="/mcore/sto/{{ $stoData->id }}/newroom" class="panel-footer" method="post">
              {{ csrf_field() }}
              <div class="form-group form-message-light">
                <label>Input Ruangan</label>
                <div class="input-group">
                  <input name="label" class="form-control" data-msg-required="Masukkan Nama/Label Ruangan" required>
                  <div class="input-group-btn">
                    <button class="btn btn-info">
                      <i class="fas fa-check"></i>
                      <span>Simpan</span>
                    </button>
                  </div>
                </div>
              </div>
            </form>
          @endif
        </div>
      </div>
    @endif
  </div>
@endsection

@section('script')
  @include('partial.mapmarker.modal')
  @include('partial.mapmarker.script', ['field' => 'coordinate', 'canEdit' => $canEdit])

  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => $canEdit
  ])

  @include('partial.form.validate', ['id' => 'sto-form'])
  @include('partial.form.validate', ['id' => 'room-form'])
@endsection
