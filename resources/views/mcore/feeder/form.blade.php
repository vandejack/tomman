@extends('app')

<?php $title = isset($feederData->id) ? $feederData->label : 'Input Feeder' ?>
@section('title', $title)

@section('body')
  @isset($feederData->id)
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/feeder/workzone/{{ $feederData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $feederData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">FEED</span>
        <span>{{ $feederData->label }}</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  @isset ($feederData->id)
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#">Data &amp; Core</a>
      </li>
      <li>
        <a href="/mcore/feeder/{{ $feederData->id }}/route">Jalur Kabel</a>
      </li>
    </ul>
  @endif

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          @include('partial.form.text', [
            'label' => 'Nama / Kode Kabel',
            'object' => $feederData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Nama Kabel'
            ]
          ])
        </div>

        <div class="col-md-4">
          @include('partial.workzone.formcontrol', [
            'label' => 'Work Zone',
            'object' => $feederData,
            'field' => 'workzone_id',
            'displayField' => 'workzone_label',
            'canEdit' => $canEdit,
            'workzoneTree' => $workzoneTree,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan pilih Area Kerja'
            ]
          ])
        </div>

        <div class="col-md-2">
          @include('partial.form.select.val', [
            'label' => 'Kapasitas Core',
            'object' => $feederData,
            'field' => 'capacity',
            'options' => $capacities,
            'canEdit' => $canEdit
          ])
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @isset ($feederData->id)
    @foreach($coreList as $tube)
      <div class="panel">
        <div class="list-group">
          @foreach($tube->coreList as $core)
            <div class="list-group-item">
              @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($core->coreId))

              @if($core->link)
                <div class="m-t-2">
                  <div class="block-md-inline">
                    <span class="label label-primary">STO</span>
                    <a href="/mcore/sto/{{ $core->link->sto_id }}">{{ $core->link->sto_label }}</a>
                  </div>

                  <div class="block-md-inline m-md-l-10">
                    <span class="label label-primary">ODF</span>
                    <a href="/mcore/sto/{{ $core->link->sto_id }}/room/{{ $core->link->sto_room_id }}/odf/{{ $core->link->odf_id }}">
                      {{ $core->link->odf_label }}
                    </a>
                  </div>

                  <div class="block-md-inline m-md-l-5">
                    <?php $otbToken = explode(':', $core->link->src_val) ?>
                    <span>port</span>
                    <span class="label label-outline label-info">{{ $otbToken[0] }}</span>
                  </div>

                  <div class="block-md-inline m-l-1">
                    <i class="fas fa-long-arrow-alt-right text-success hidden-xs"></i>
                    <i class="fas fa-long-arrow-alt-down text-success visible-xs-inline"></i>
                  </div>

                  <div class="block-md-inline m-md-l-10">
                    <span class="label label-primary">ODC</span>
                    <a href="/mcore/odp/{{ $core->link->odc_id }}">{{ $core->link->odc_label }}</a>
                  </div>

                  <div class="block-md-inline m-md-l-10">
                    <?php $odcToken = explode(':', $core->link->dst_val) ?>
                    <span>panel</span>
                    <span class="label label-outline label-info">{{ $odcToken[0] }}</span>
                    <span>port</span>
                    <span class="label label-outline label-info">{{ $odcToken[1] }}</span>
                  </div>
                </div>
              @endif
            </div>
          @endforeach
        </div>
      </div>
    @endforeach
  @endif
@endsection

@section('script')
  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => $canEdit
  ])

  @include('partial.form.validate', ['id' => 'form'])
@endsection
