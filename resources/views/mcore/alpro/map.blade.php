@extends('app')

@section('title', 'Input Alpro')

@section('style')
  <style>
    .px-content {
      padding: 0;
    }
    #loading-indicator {
      position: absolute;
      top: 20px;
      width: 100%;
      text-align: center;
    }
    #loading-indicator > span {
      padding: 4px 8px;
      background-color: #cc3e00;
    }

    .gbtn {
      font-family: Roboto, Arial, sans-serif;
      user-select: none;
      background-color: white;
      color: black;
      border-radius: 2px;
      box-shadow: rgba(0, 0, 0, 0.3) 0 1px 4px -1px;
      border: none;
    }
    #btn-start-input {
      position: absolute;
      right: 10px;
      top: 60px;
      font-size: 18px;
      font-weight: 500;
      padding: 8px 23px;
      line-height: 1.2;
    }
    #btn-center {
      position: absolute;
      right: 10px;
      bottom: 141px;
      font-size: 18px;
      padding: 8px 11px;
    }

    #frm-alpro {
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      padding: 10px 20px 0;
      background-color: #2d353d;
    }

    #container-alert {
      position: absolute;
      top: 115px;
      left: 10px;
      right: 10px;
      z-index: 1;
    }
  </style>
@endsection

@section('body')
  <div id="alpro_map"></div>

  <div id="loading-indicator" class="hidden">
    <span>Loading...</span>
  </div>

  <div>
    <button id="btn-start-input" class="gbtn" type="button">
      <i class="fas fa-plus"></i>
      <span>Alpro</span>
    </button>

    <button id="btn-center" class="gbtn" type="button">
      <i class="fas fa-crosshairs"></i>
    </button>
  </div>

  <form id="frm-alpro" class="hidden" method="post">
    {{ csrf_field() }}

    @include('partial.form.text', [
      'label' => 'Koordinat',
      'object' => null,
      'field' => 'coordinate',
      'options' => [],
      'canEdit' => true,
      'attributes' => [
        'required' => true,
        'data-msg-required' => 'Silahkan isi Koordinat'
      ]
    ])

    @include('partial.form.select.keyval', [
      'label' => 'Tipe Alpro',
      'object' => null,
      'field' => 'type',
      'options' => $types,
      'canEdit' => true
    ])

    @include('partial.workzone.formcontrol', [
      'label' => 'Work Zone',
      'object' => null,
      'field' => 'workzone_id',
      'displayField' => 'workzone_label',
      'canEdit' => true,
      'workzoneTree' => $workzoneTree,
      'attributes' => [
        'required' => true,
        'data-msg-required' => 'Silahkan pilih Area Kerja'
      ]
    ])

    <div class="form-group">
      <button class="btn btn-primary pull-right">
        <i class="fas fa-check"></i>
        <span>Simpan</span>
      </button>

      <button id="btn-cancel" class="btn btn-link btn-alert" type="reset">
        <i class="fas fa-ban"></i>
        <span>Batal</span>
      </button>
    </div>
  </form>
@endsection

@section('script')
  @include('mcore.map.script')
  @include('mcore.map.const')
  @include('mcore.map.grid')
  @include('mcore.alpro.grid')
  @include('mcore.alpro.init')
  @include('partial.googlemap', ['callback' => 'Alpro.init'])

  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => true
  ])
  @include('partial.form.validate', ['id' => 'frm-alpro'])
@endsection
