@extends('app')

@section('title', 'ODP '.$workzoneData->label)

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/odp/workzone/{{ $workzoneData->id }}">
        <span class="label label-primary">WZ</span>
        <span>{{ $workzoneData->label }}</span>
      </a>
    </li>
    <li class="active">
      ODP
    </li>
  </ol>

  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>{{ $workzoneData->label }}</span>
    </h1>
  </div>

  <div class="row m-b-4">
    <div class="col-md-6">
      @if ($canCreateNew)
        <a href="/mcore/odp/new" class="btn btn-info width-xs-full m-xs-b-20">
          <i class="fas fa-plus"></i>
          <span>Input ODP</span>
        </a>
      @endif
    </div>
    <div class="col-md-3 col-md-push-3">
      <form>
        <div class="input-group">
          <?php $q = Request::query('q') ?>
          <input name="q" class="form-control" value="{{ $q }}">
          <span class="input-group-btn">
            <button class="btn"><i class="fas fa-search"></i></button>
            @isset($q)
              <a href="/mcore/odp/workzone/{{ $workzoneData->id }}" class="btn btn-link">Clear</a>
            @endisset
          </span>
        </div>
      </form>
    </div>
  </div>

  <ul class="list-blocks clearfix">
    @foreach($odpList as $odp)
      <li>
        <a href="/mcore/odp/{{ $odp->id }}">{{ $odp->label }}</a>
      </li>
    @endforeach
  </ul>

  @if ($odpList->total() > $odpList->perPage())
    <div class="text-center">
      {{ $odpList->links() }}
    </div>
  @endif
@endsection
