@extends('app')

@section('title', 'ODP')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>ODP by WorkZone</span>
    </h1>

    @if ($canCreateNew)
      <a href="/mcore/odp/new" class="btn btn-info pull-right">
        <i class="fas fa-plus"></i>
        <span>Input ODP</span>
      </a>
    @endif
  </div>

  <ul class="tree tree-links">
    @each('partial.treelink', $workzoneTree, 'tree')
  </ul>
@endsection
