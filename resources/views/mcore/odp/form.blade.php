@extends('app')

<?php if (!isset($odpData)) $odpData = null ?>
<?php $title = $odpData->label ?? 'Input ODP' ?>

@section('title', $title)

@section('body')
  @isset($odpData->id)
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/odp/workzone/{{ $odpData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $odpData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">ODP</span>
        <span>{{ $odpData->label }}</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          @include('partial.form.text', [
            'label' => 'Nama',
            'object' => $odpData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Nama ODP'
            ]
          ])
        </div>

        <div class="col-md-4">
          @include('partial.form.text', [
            'label' => 'Nama / Kode TENOSS',
            'object' => $odpData,
            'field' => 'tenoss',
            'canEdit' => $canEdit
          ])
        </div>
        
        <div class="col-md-4">
          @include('partial.workzone.formcontrol', [
            'label' => 'Work Zone',
            'object' => $odpData,
            'field' => 'workzone_id',
            'displayField' => 'workzone_label',
            'canEdit' => $canEdit,
            'workzoneTree' => $workzoneTree,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan pilih Area Kerja'
            ]
          ])
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          @include('partial.mapmarker.formcontrol', [
            'label' => 'Koordinat (lat, lng)',
            'object' => $odpData,
            'field' => 'coordinate',
            'canEdit' => $canEdit
          ])
        </div>

        <div class="col-md-4">
          @include('partial.form.select.keyval', [
            'label' => 'Tipe',
            'object' => $odpData,
            'field' => 'type',
            'options' => $types,
            'canEdit' => $canEdit
          ])
        </div>

        <div class="col-md-2">
          @include('partial.form.select.keyval', [
            'label' => 'Status',
            'object' => $odpData,
            'field' => 'status',
            'options' => $statuses,
            'canEdit' => $canEdit
          ])
        </div>

        <div class="col-md-2">
          @include('partial.form.select.val', [
            'label' => 'Kapasitas',
            'object' => $odpData,
            'field' => 'capacity',
            'options' => $capacities,
            'canEdit' => $canEdit
          ])
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @isset($odpData->id)
    <div class="panel">
      <div class="panel-body">
        @isset($sourceLink)
          <div class="m-b-3">
            <div>
              <span class="label label-primary">ODC</span>
              <a href="/mcore/odc/{{ $sourceLink->odc_id }}">{{ $sourceLink->odc_label }}</a>

              <?php $odcToken = explode(':', $sourceLink->src_val) ?>
              <span class="m-l-1">panel</span>
              <span class="label label-outline label-info">{{ $odcToken[0] }}</span>
              <span>port</span>
              <span class="label label-outline label-info">{{ $odcToken[1] }}</span>
            </div>
            <div class="m-b-1 m-t-1 m-l-3 text-success"><i class="fas fa-long-arrow-alt-down"></i></div>
            <div class="m-b-1">
              <span class="label label-primary">DIST</span>
              <a href="/mcore/distribusi/{{ $sourceLink->distribusi_id }}">{{ $sourceLink->distribusi_label }}</a>
            </div>

            @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($sourceLink->med_val))
          </div>
        @endif

        <a href="/mcore/odp/{{ $odpData->id }}/dist" class="btn">
          <i class="fas fa-link"></i>
          <span>Distribusi</span>
        </a>
      </div>
    </div>

    <div class="panel odp-port">
      <div class="panel-heading">
        <div class="panel-title">Port</div>
      </div>

      <ul class="list-group">
        @foreach($ports as $port => $link)
            <li class="list-group-item">
              <span class="port {{ $link ? 'up' : '' }}">{{ $port }}</span>

              @isset($link->pelanggan_id)
                <a href="/mcore/pelanggan/{{ $link->pelanggan_id }}">
                  <span class="label label-outline label-success">{{ $link->pelanggan_kode }}</span>
                  <span>{{ $link->pelanggan_label }}</span>
                </a>

                @if ($link->med_val)
                  <span>via</span>
                  <span>{{ $link->med_val }}</span>
                @endif
              @endisset

              <a href="/mcore/odp/{{ $odpData->id }}/port/{{ $port }}" class="btn btn-default pull-md-right">
                <i class="fas fa-link"></i>
                <span>Pelanggan</span>
              </a>
            </li>
        @endforeach
      </ul>
    </div>
  @endisset

@endsection

@section('script')
  @include('partial.mapmarker.modal')
  @include('partial.mapmarker.script', ['field' => 'coordinate', 'canEdit' => $canEdit])

  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => $canEdit
  ])

  @include('partial.form.validate', ['id' => 'form'])
@endsection
