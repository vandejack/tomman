<script>
  $(function() {
    const $splitterList = window.$splitterList;

    const $splitterModal = window.$splitterModal;
    const $splitterModalTitle = window.$splitterModalTitle;
    const $splitterModalBody = window.$splitterModalBody;

    const $inputSplitterEditId = window.$inputSplitterEditId;
    const $inputSplitterEditLabel = window.$inputSplitterEditLabel;

    let $currentSplitterHidden;
    let $currentSplitterTitle;
    let $currentSplitterBody;

    $splitterList.on('click', 'li', event => {
      const $target = $(event.currentTarget);

      const id = $target.data('splitter-id');
      $inputSplitterEditId.val(id);

      const label = $target.data('splitter-label');
      $inputSplitterEditLabel.val(label);

      $currentSplitterHidden = $target.children('.hidden');
      $currentSplitterTitle = $currentSplitterHidden.children('.title').detach();
      $currentSplitterBody = $currentSplitterHidden.children('.body').detach();

      $splitterModalTitle.empty().append($currentSplitterTitle);
      $splitterModalBody.empty().append($currentSplitterBody);

      $splitterModal.modal('show');
    });

    $splitterModal.on('hidden.bs.modal', event => {
      $currentSplitterTitle.detach().appendTo($currentSplitterHidden);
      $currentSplitterBody.detach().appendTo($currentSplitterHidden);
    });

    $splitterModal.on('click', '.clickable', event => {
      const $target = $(event.currentTarget);
      const panel = $target.data('odc-panel');
      const port = $target.data('odc-port');

      $splitterModal.modal('hide');
      $(`#panel-${panel}-port-${port}-front`).click();
    });
  });
</script>
