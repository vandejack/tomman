<script>
  $(function() {
    const $linkModal = window.$linkModal;
    const $linkModalTitle = window.$linkModalTitle;

    const $formSplitter = window.$formSplitter;
    const $containerSplitter = window.$containerSplitter;
    const $inputSplitter = window.$inputSplitter;
    const $inputSplitterLabel = window.$inputSplitterLabel;
    const $inputSplitterPort = window.$inputSplitterPort;

    const $labelSplitterNew = window.$labelSplitterNew;
    const $inputSplitterNew = window.$inputSplitterNew;
    const $checkSplitterNew = window.$checkSplitterNew;

    const $inputOdcPanel = window.$inputOdcPanel;
    const $inputOdcPort = window.$inputOdcPort;

    const $containerSplitterPort = window.$containerSplitterPort;
    const $textSplitterDescriptor = window.$textSplitterDescriptor;

    const $readonlySplitterLabel = window.$readonlySplitterLabel;
    const $readonlySplitterPort = window.$readonlySplitterPort;

    const $btnFrontLinkSave = window.$btnFrontLinkSave;
    const $btnFrontLinkUnplug = window.$btnFrontLinkUnplug;

    const $panelPorts = window.$panelPorts;

    const hiddenClass = window.hiddenClass;
    const escapeMarkup = window.escapeMarkup;

    let defaultPorts = [];
    const fillDefaultSplitterPort = () => {
      if (!defaultPorts.length) {
        defaultPorts.push({
          id: 0,
          text: `
            <span class="label label-info">in</span>
            <span class="label label-success label-outline">IN</span>
            <i class="fas fa-long-arrow-alt-left "></i>
            <span>KOSONG</span>
          `
        });
        defaultPorts.push({
          id: -1,
          text: '<hr>',
          disabled: true
        });

        for (let i = 1; i <= 4; i++) {
          defaultPorts.push({
            id: i,
            text: `
              <span class="label label-info">out</span>
              <span class="label label-success label-outline">${i}</span>
              <i class="fas fa-long-arrow-alt-left "></i>
              <span>KOSONG</span>
            `
          });
        }
      }

      if ($inputSplitterPort.hasClass('select2-hidden-accessible')) {
        $inputSplitterPort.select2('destroy').empty();
      }

      $inputSplitterPort
        .select2({
          data: defaultPorts,
          placeholder: '&nbsp',
          disabled: false,
          dropdownParent: $linkModal,
          escapeMarkup: escapeMarkup
        });
    };

    const clearSplitterPort = () => {
      if ($inputSplitterPort.hasClass('select2-hidden-accessible')) {
        $inputSplitterPort
          .val(null)
          .trigger('change')
          .select2('destroy')
          .empty()
        ;
      }

      const currentSelection = $inputSplitter.select2('data');
      const options = {
        placeholder: '&nbsp;',
        dropdownParent: $linkModal,
        escapeMarkup: escapeMarkup
      };
      if (currentSelection.length) {
        options.data = currentSelection[0].ports;
      } else {
        options.disabled = true;
      }

      $inputSplitterPort.select2(options);
    };

    const switchSplitterForm = showNewSplitterForm => {
      if (showNewSplitterForm) {
        $labelSplitterNew.removeClass(hiddenClass);

        $inputSplitterLabel.removeClass(hiddenClass);
        $containerSplitter.addClass(hiddenClass);

        $inputSplitter.prop('required', false);
        $inputSplitterLabel.prop('required', true).focus();
        fillDefaultSplitterPort();
      }
      else {
        $labelSplitterNew.addClass(hiddenClass);

        $inputSplitterLabel.addClass(hiddenClass);
        $containerSplitter.removeClass(hiddenClass);

        $inputSplitter.prop('required', true);
        $inputSplitterLabel.prop('required', false);
        clearSplitterPort();
      }

      $formSplitter.validate().resetForm();
    };

    let $currentPortHidden;
    let $currentPortLinkSplitter;
    let $currentPortLinkSplitterPort;

    $panelPorts.on('click', '.port', event => {
      const $target = $(event.currentTarget);

      const panel = $target.data('panel');
      const port = $target.data('port');

      $inputOdcPanel.val(panel);
      $inputOdcPort.val(port);

      $inputSplitterLabel.val('');
      $inputSplitterNew.prop('checked', false).change();

      $linkModalTitle.text(`Panel ${panel} Port ${port}`);

      if ($target.hasClass('up')) {
        $containerSplitter.addClass(hiddenClass);
        $containerSplitterPort.addClass(hiddenClass);
        $textSplitterDescriptor.addClass(hiddenClass);
        $checkSplitterNew.addClass(hiddenClass);
        $btnFrontLinkSave.addClass(hiddenClass);

        $btnFrontLinkUnplug.removeClass(hiddenClass);
        $readonlySplitterLabel.removeClass(hiddenClass);
        $readonlySplitterPort.removeClass(hiddenClass);

        $currentPortHidden = $target.children('.hidden');
        $currentPortLinkSplitter = $currentPortHidden.children('.splitter-view').detach();
        $currentPortLinkSplitterPort = $currentPortHidden.children('.splitter-port-view').detach();

        $readonlySplitterLabel.empty().append($currentPortLinkSplitter);
        $readonlySplitterPort.empty().append($currentPortLinkSplitterPort);
      } else {
        $currentPortHidden = $currentPortLinkSplitter = $currentPortLinkSplitterPort = null;

        $checkSplitterNew.removeClass(hiddenClass);
        $containerSplitterPort.removeClass(hiddenClass);
        $btnFrontLinkSave.removeClass(hiddenClass);

        $btnFrontLinkUnplug.addClass(hiddenClass);
        $readonlySplitterLabel.addClass(hiddenClass);
        $readonlySplitterPort.addClass(hiddenClass);

        clearSplitterPort();
      }

      $linkModal.modal('show');
    });

    $linkModal.on('hidden.bs.modal', event => {
      if (!$currentPortHidden) return;

      $currentPortLinkSplitter.detach().appendTo($currentPortHidden);
      $currentPortLinkSplitterPort.detach().appendTo($currentPortHidden);
    });

    $inputSplitterNew.on('change', () => {
      switchSplitterForm($inputSplitterNew.is(':checked'))
    });

    $inputSplitter.on('select2:select', clearSplitterPort);

    $inputSplitter.select2({
      data: window.splitterList,
      placeholder: '&nbsp;',
      dropdownParent: $linkModal,
      escapeMarkup: escapeMarkup
    });

    $inputSplitterNew.prop('checked', false);
  });
</script>
