<script>
  window.splitterList = [
    @foreach($splitterList as $splitter)
      {
        id: '{{ $splitter->id }}',
        text: `
          <span>Splitter</span>
          <span class="label bg-fiber-{{ $splitter->colorIndex }}">{{ $splitter->label }}</span>
        `,
        ports: [
          {
            id: 0,
            text: `
              <span class="label label-success">in</span>
              <span class="label label-info label-outline">IN</span>
              <i class="fas fa-long-arrow-alt-left "></i>
              @isset ($splitter->ports[0])
                <?php $token = explode(':', $splitter->ports[0]->src_val) ?>
                <span>Panel</span>
                <span class="label label-info label-outline">{{ $token[0] }}</span>
                <span>Port</span>
                <span class="label label-info label-outline">{{ $token[1] }}</span>
              @else
                <span>KOSONG</span>
              @endisset
            `,
            disabled: {{ isset($splitter->ports[0]) ? 'true' : 'false' }}
          },
          {
            id: -1,
            text: '<hr>',
            disabled: true
          },
          @for ($i = 1; $i <= 4; $i++)
            {
              id: '{{ $i }}',
              text: `
                <span class="label label-success">out</span>
                <span class="label label-info label-outline">{{ $i }}</span>
                <i class="fas fa-long-arrow-alt-right "></i>
                @isset ($splitter->ports[$i])
                  <?php $token = explode(':', $splitter->ports[$i]->dst_val) ?>
                  <span>Panel</span>
                  <span class="label label-info label-outline">{{ $token[0] }}</span>
                    <span>Port</span>
                    <span class="label label-info label-outline">{{ $token[1] }}</span>
                  @else
                  <span>KOSONG</span>
                @endisset
              `,
              disabled: {{ isset($splitter->ports[$i]) ? 'true' : 'false' }}
            },
          @endfor
        ]
      },
    @endforeach
  ];
</script>
