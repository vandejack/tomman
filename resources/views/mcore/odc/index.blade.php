@extends('app')

@section('title', 'ODC')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>ODC by WorkZone</span>
    </h1>

    @if ($canCreateNew)
      <a href="/mcore/odc/new" class="btn btn-info pull-right">
        <i class="fas fa-plus"></i>
        <span>Input ODC</span>
      </a>
    @endif
  </div>

  <ul class="tree tree-links">
    @each('partial.treelink', $workzoneTree, 'tree')
  </ul>
@endsection
