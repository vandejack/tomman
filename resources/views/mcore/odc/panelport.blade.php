<div id="panelports" class="overflow-h m-t-4">
  @foreach($panelPorts as $panelNum => $panel)
    <div class="odc-panel flex-h">
      <div class="caption">
        PANEL {{ $panelNum }}
      </div>

      @foreach($panel as $port => $link)
        <div class="port-column">

          <div id="panel-{{ $panelNum }}-port-{{ $port }}-rear"
               class="rearport {{ $link->rear ? 'up' : '' }}"
               data-port="{{ $port }}" data-panel="{{ $panelNum }}"
               data-link-dst="{{ $link->rear->dst_type ?? '' }}">
            <i class="fas fa-sort-up"></i>

            @if ($link->rear)
              <div class="hidden">
                <div class="link-view">
                  @if ($link->rear->dst_type == \App\Service\Mcore\Helper::TYPE_ODP)

                    <div class="m-b-1">
                      <span class="label label-primary">DIST</span>
                      <a href="/mcore/distribusi/{{ $link->rear->distribusi_id }}">
                        {{ $link->rear->distribusi_label }}
                      </a>
                    </div>
                    <div class="m-b-1">
                      @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($link->rear->med_val))
                    </div>

                    <div class="m-b-1 m-l-3"><i class="fas fa-long-arrow-alt-down"></i></div>

                    <div>
                      <span class="label label-primary">ODP</span>
                      <a href="/mcore/odp/{{ $link->rear->odp_id }}">
                        {{ $link->rear->odp_label }}
                      </a>
                    </div>

                  @elseif ($link->rear->src_type == \App\Service\Mcore\Helper::TYPE_ODF_PANEL)

                    <div class="m-b-1">
                      <div class="block-md-inline">
                        <span class="label label-primary">STO</span>
                        <?php $url = '/mcore/sto/'.$link->rear->sto_id ?>
                        <a href="{{ $url }}">
                          {{ $link->rear->sto_label }}
                        </a>
                      </div>

                      <div class="block-md-inline m-md-l-5">
                        <span class="label label-outline label-primary">Room</span>
                        <?php $url .= '/room/'.$link->rear->sto_room_id ?>
                        <a href="{{ $url }}">
                          {{ $link->rear->sto_room_label }}
                        </a>
                      </div>
                    </div>

                    <div class="m-b-1">
                      <span class="label label-primary">ODF</span>
                      <?php $url .= '/odf/'.$link->rear->odf_id ?>
                      <a href="{{ $url }}">
                        {{ $link->rear->odf_label }}
                      </a>
                    </div>

                    <div class="m-b-1">
                      <div class="block-md-inline">
                        <span class="label label-primary">OTB</span>
                        <?php $url .= '/panel/'.$link->rear->odf_panel_id ?>
                        <a href="{{ $url }}">
                          {{ $link->rear->odf_panel_label }}
                        </a>
                      </div>
                      <div class="block-md-inline m-md-l-5">
                        <span class="label label-outline label-primary">Port</span>
                        <?php $token = explode(':', $link->rear->src_val) ?>
                        <span class="label label-outline label-info">{{ $token[0] }}</span>
                      </div>
                    </div>

                    <div class="m-b-1 m-l-3 text-success"><i class="fas fa-long-arrow-alt-down"></i></div>

                    <div class="m-b-1">
                      <span class="label label-primary">FEED</span>
                      <a href="/mcore/feeder/{{ $link->rear->feeder_id }}">
                        {{ $link->rear->feeder_label }}
                      </a>
                    </div>
                    <div class="m-b-1">
                      @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($link->rear->med_val))
                    </div>

                  @endif

                  <a href="/mcore/odc/{{ $odcData->id }}/panel/{{ $panelNum }}/port/{{ $port }}"
                     class="btn m-t-4">
                    <i class="fas fa-link"></i>
                    <span>Detail Sambungan</span>
                  </a>

                </div>
              </div>
            @endif
          </div>

          <?php $class = '' ?>
          @if ($link->front)
            <?php $class = 'bg-fiber-'.$link->front->dst_obj->colorIndex ?>
            @if ($link->front->dst_type === \App\Service\Mcore\Helper::TYPE_ODC_SPLIITER)
              <?php $class .= ' in' ?>
            @endif
          @endif
          <div id="panel-{{ $panelNum }}-port-{{ $port }}-front"
               class="port {{ $class }}"
               data-port="{{ $port }}" data-panel="{{ $panelNum }}">
            {{ $port }}

            @if ($link->front)
              <div class="hidden">
                <div class="splitter-view">
                  <span class="label bg-fiber-{{ $link->front->dst_obj->colorIndex }}">
                    {{ $link->front->dst_obj->label }}
                  </span>
                </div>
                <div class="splitter-port-view">
                  @if ($link->front->dst_val <= 0)
                    <?php $splitterPortType = 'in'; $splitterPortLabel = 'IN' ?>
                  @else
                    <?php $splitterPortType = 'out'; $splitterPortLabel = $link->front->src_val ?>
                  @endif
                  <span class="label label-success">{{ $splitterPortType }}</span>
                  <span>port</span>
                  <span class="label label-outline label-info">{{ $splitterPortLabel }}</span>
                </div>
              </div>
            @endif
          </div>
        </div>
      @endforeach
    </div>
  @endforeach
</div>
