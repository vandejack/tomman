@extends('app')

@section('title', $entityData->label)

@section('style')
  <style>
    #container-map {
      position: relative;
    }
    #loading-indicator {
      position: absolute;
      top: 20px;
      width: 100%;
      text-align: center;
    }
    #loading-indicator > span {
      padding: 4px 8px;
      background-color: #cc3e00;
    }

    #point-view {
      display: flex;
      flex-direction: column;
    }
    #point-view > ul, #point-view > div {
      flex-grow: 1;
      padding-left: 0;
      padding-top: 10px;
      overflow-y: auto;
    }

    #point-view .btn-primary {
      display: none;
    }

    #point-view.mode-edit .btn-primary {
      display: inline-block;
    }

    #point-view.mode-edit .btn-info {
      display: none;
    }

    #point-view .btn-danger {
      font-size: 18px;
      padding: 0 8px;
      margin-top: 8px;
    }

    .draghandler {
      font-size: 24px;
      font-weight: bold;
      padding-right: 10px;
      cursor: move;
      line-height: 1.2;
    }

    #point-view li {
      padding: 4px 8px;
      cursor: default;
    }
    .highlight {
      background-color: rgba(0, 0, 0, 0.1);
    }
  </style>
@endsection

@section('body')
  @isset($entityData->id)
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/{{ $entityData->type }}/workzone/{{ $entityData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $entityData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">{{ $entityData->shortLabel }}</span>
        <span>{{ $entityData->label }}</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $entityData->label }}</span>
    </h1>
  </div>

  <ul class="nav nav-tabs">
    <li>
      <a href="/mcore/{{ $entityData->type }}/{{ $entityData->id }}">Data &amp; Core</a>
    </li>
    <li class="active">
      <a href="#">Jalur Kabel</a>
    </li>
  </ul>

  <form method="post" id="form" class="panel">
    {{ csrf_field() }}
    <input id="input-route_path" name="route_path" type="hidden">

    <div class="panel-body">
      <div class="row">
        <div id="container-map" class="col-md-8">
          <div id="map-view"></div>

          <div id="loading-indicator" class="hidden">
            <span>Loading...</span>
          </div>
        </div>

        <div class="col-md-4">
          <div id="point-view" v-bind:class="{ 'mode-edit': isEditing }">
            <div v-if="!(path.length)">Belum ada data</div>
            <ul class="list-unstyled">
              <li v-for="(item, index) in path" :key="item.deviceType+item.id"
                  v-bind:class="{ 'highlight': item.isHighlight }"
                  v-on:click="setHighlight(item)">
                <div v-bind:class="{ 'hidden': !isEditing }" class="draghandler pull-left">
                  <!--<i class="fas fa-sort"></i>-->
                  &#9776;
                </div>
                <div>
                  <button v-on:click="remove(index)"
                          v-bind:class="{ 'hidden': !isEditing }"
                          class="btn btn-xs btn-danger pull-right"
                          type="button">&times;</button>
                  <div>
                    <div>
                      <span v-if="isSto(item.deviceType)">
                        <span class="label label-primary">STO</span>
                        <a v-bind:href="'/mcore/sto/' + item.id">@{{ item.label }}</a>
                      </span>

                      <span v-else-if="isOdc(item.deviceType)">
                        <span class="label label-primary">ODC</span>
                        <a v-bind:href="'/mcore/odc/' + item.id">@{{ item.label }}</a>
                      </span>

                      <span v-else-if="isOdp(item.deviceType)">
                        <span class="label label-primary">ODP</span>
                        <a v-bind:href="'/mcore/odp/' + item.id">@{{ item.label }}</a>
                      </span>

                      <span v-else-if="isPelanggan(item.deviceType)">
                        <span class="label label-success">@{{ item.kode }}</span>
                        <a v-bind:href="'/mcore/pelanggan/' + item.id">@{{ item.label }}</a>
                      </span>

                      <span v-else-if="isAlpro(item.deviceType)">
                        <span class="label label-primary">alpro</span>
                        <span>@{{ item.typeAsText }}</span>
                      </span>
                    </div>
                    <div>
                      <span>@{{ item.lat }}</span>
                      <span>,</span>
                      <span>@{{ item.lng }}</span>
                    </div>
                  </div>
                </div>
              </li>
            </ul>

            <button class="btn btn-primary pull-right">
              <i class="fas fa-check"></i>
              <span>Simpan</span>
            </button>

            @if ($canEdit)
              <button v-on:click="beginEdit()" class="btn btn-info pull-right" type="button">
                <i class="fas fa-edit"></i>
                <span>Edit</span>
              </button>
            @endif
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('script')
  <script>window.route = {}</script>
  @if ($routeData)
    <script>
      window.route.path = {!! json_encode($routeData) !!}
    </script>
  @endif

  @include('mcore.map.script')
  @include('mcore.map.const')
  @include('mcore.map.grid')
  @include('mcore.map.features.sto')
  @include('mcore.map.features.grid')
  @include('mcore.alpro.grid')
  @include('mcore.route.map')
  @include('mcore.route.polyline')
  @include('mcore.route.viewmodel')
  @include('partial.googlemap', ['callback' => 'route.viewModel.init'])

  <script>
    $(() => {
      const $form = $('#form');
      $form.submit(() => {
        $form.addClass('form-loading form-loading-inverted');
        const path = window.route.viewModel.getPath();
        $('#input-route_path').val(JSON.stringify(path));
      });
    })
  </script>
@endsection
