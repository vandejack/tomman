@extends('app')

@section('title', 'Kabel Distribusi')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>Kabel Distribusi per WorkZone</span>
    </h1>

    @if ($canCreateNew)
      <a href="/mcore/distribusi/new" class="btn btn-info pull-right">
        <i class="fas fa-plus"></i>
        <span>Input Distribusi</span>
      </a>
    @endif
  </div>

  <ul class="tree tree-links">
    @each('partial.treelink', $workzoneTree, 'tree')
  </ul>
@endsection
