@extends('app')

@section('title', 'Kabel Distribusi '.$workzoneData->label)

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/distribusi/workzone/{{ $workzoneData->id }}">
        <span class="label label-primary">WZ</span>
        <span>{{ $workzoneData->label }}</span>
      </a>
    </li>
    <li class="active">
      Kabel Distribusi
    </li>
  </ol>

  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>{{ $workzoneData->label }}</span>
    </h1>
  </div>

  <div class="row m-b-4">
    <div class="col-md-6">
      @if ($canCreateNew)
        <a href="/mcore/distribusi/new" class="btn btn-info width-xs-full m-xs-b-20">
          <i class="fas fa-plus"></i>
          <span>Input Distribusi</span>
        </a>
      @endif
    </div>
    <div class="col-md-3 col-md-push-3">
      <form>
        <div class="input-group">
          <?php $q = Request::query('q') ?>
          <input name="q" class="form-control" value="{{ $q }}">
          <span class="input-group-btn">
            <button class="btn"><i class="fas fa-search"></i></button>
            @isset($q)
              <a href="/mcore/distribusi/workzone/{{ $workzoneData->id }}" class="btn btn-link">Clear</a>
            @endisset
          </span>
        </div>
      </form>
    </div>
  </div>

  <ul class="list-blocks clearfix">
    @foreach($distribusiList as $distribusi)
      <li>
        <a href="/mcore/distribusi/{{ $distribusi->id }}">{{ $distribusi->label }}</a>
      </li>
    @endforeach
  </ul>

  @if ($distribusiList->total() > $distribusiList->perPage())
    <div class="text-center">
      {{ $distribusiList->links() }}
    </div>
  @endif
@endsection
