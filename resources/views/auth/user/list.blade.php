@extends('app')

@section('title', 'User List')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-users-cog"></i>
      <span>Users</span>
    </h1>

    @if ($canCreateNew)
      <a href="/auth/user/new" class="btn btn-info pull-right">
        <i class="fas fa-plus"></i>
        <span>Create Local User</span>
      </a>
    @endif
  </div>

  <div class="row">
    @if ($newUsers !== false)
      <?php $class = count($newUsers) ? '' : 'hidden-xs' ?>
      <div class="col-md-6 {{ $class }}">
        <div class="panel">
          <div class="panel-heading">
            <div class="panel-title">Newly Registered</div>
          </div>
          <div class="panel-body">
            @if (!count($newUsers))
              <span>NO DATA</span>
            @else
              <div class="list-group">
                @foreach($newUsers as $entry)
                <a href="/auth/user/{{ $entry->id }}" class="list-group-item">
                  <h4 class="list-group-item-heading">{{ $entry->login }}</h4>
                  <span>{{ $entry->nama }}</span>
                </a>
                @endforeach
              </div>
            @endif
          </div>
        </div>
      </div>
    @endif

    <?php $class = ($newUsers === false) ? 'col-md-push-3' : '' ?>
    <div class="col-md-6 {{ $class }}">
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">
            @if ($currentUser->workzone_id)
              {{ $currentUser->workzone_label }}
            @else
              &nbsp;
            @endif
          </span>
          <div class="panel-heading-controls col-sm-4">
            <form>
              <div class="input-group input-group-sm">
                <input name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button class="btn">
                    <i class="fas fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
          </div>
        </div>

        <div class="panel-body">
          @if (!count($listUsers))
            <span>NO DATA</span>
          @else
            <div class="list-group">
              @foreach($listUsers as $entry)
                {{-- skip current user --}}
                @if ($entry->id == $currentUser->id)
                  @continue
                @endif

                <a href="/auth/user/{{ $entry->id }}" class="list-group-item">
                  <h4 class="list-group-item-heading">
                    {{ $entry->login }}
                    <span class="label label-info">{{ $entry->workzone_label }}</span>
                  </h4>
                  <span>{{ $entry->nama }}</span>
                </a>
              @endforeach
            </div>
          @endif
        </div>

        @if ($listUsers->total() > $listUsers->perPage())
          <div class="panel-footer text-center">
            {{ $listUsers->links() }}
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
