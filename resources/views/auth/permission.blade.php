@extends('app')

@section('title', 'Permission')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-user-shield"></i>
      <span>Permission Group</span>
    </h1>
  </div>

  <div class="row">
    <div class="col-md-6 {{ $isWritable ? '' : 'col-md-push-3' }}">
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">Permission Group</span>
          <div class="panel-heading-controls col-sm-4">
            <form>
              <div class="input-group input-group-sm">
                <input name="q" class="form-control" placeholder="Search..." value="{{ $q }}">
                <span class="input-group-btn">
                  <button class="btn">
                    <i class="fas fa-search"></i>
                  </button>

                  @if ($q)
                    <a href="/auth/permission" class="btn btn-link">
                      Clear
                    </a>
                  @endif
                </span>
              </div>
            </form>
          </div>
        </div>

        <div class="panel-body">
          @if (!$list || !$list->count())
            <span>NO DATA</span>
          @else
            <div class="permission-list list-group">
              @foreach($list as $entry)
                @if ($isWritable)
                  <a href="#" class="list-group-item"
                     data-id="{{ $entry->id }}"
                     data-title="{{ $entry->title }}"
                     data-permission="{{ $entry->permission }}"
                  >
                @else
                  <span class="list-group-item">
                @endif

                  <h4 class="list-group-item-heading">{{ $entry->title }}</h4>
                  <?php $perms = \App\Http\Controllers\Auth\PermissionController::parseForDisplay($entry->permission) ?>
                  @foreach($perms as $perm)
                    <div class="sidetag">
                      <span class="label">{{ $perm->module }}</span>
                      @foreach($perm->permission as $permission)
                        <span class="label label-tag label-{{ $permission->class }}">
                          {{ $permission->label }}
                        </span>
                      @endforeach
                    </div>
                  @endforeach

                @if (!$isWritable)
                  </span>
                @else
                  </a>
                @endif
              @endforeach
            </div>
          @endif
        </div>

        @if ($list->total() > $list->perPage())
          <div class="panel-footer text-center">
            {{ $list->links() }}
          </div>
        @endif
      </div>
    </div>

    @if ($isWritable)
      <div class="col-md-6">
        <form method="post" class="panel">
          {{ csrf_field() }}
          <div class="panel-heading">
            <span class="panel-title">Add Permission Group</span>
          </div>
          <div class="panel-body">
            <fieldset class="form-group">
              <label for="new-title">Keterangan</label>
              <input id="new-title" name="title" type="text" class="form-control" value="{{ !old('id')?old('title'):'' }}" required>
            </fieldset>

            <?php $hasError = ($errors->has('permission') && !old('id'))?>
            <fieldset class="form-group {{ $hasError?'has-error':'' }}">
              <label for="new-permission">Permission</label>
              <textarea id="new-permission" name="permission" class="form-control" required>{{ !old('id')?old('permission'):'' }}</textarea>
              @if ($hasError)
                @foreach($errors->get('permission') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </fieldset>
          </div>
          <div class="panel-footer text-right">
            <button class="btn btn-info">
              <span>Add</span>
            </button>
          </div>
        </form>
      </div>
    @endif
  </div>

  @if ($isWritable)
    <div id="updateModal" class="modal" tabindex="-1" role="dialog">
      <form class="modal-dialog" method="post">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <input id="update-id" name="id" type="hidden" value="{{ old('id') }}">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
            <h4 class="modal-title">Edit Permission</h4>
          </div>
          <div class="modal-body">
            <fieldset class="form-group">
              <label for="update-title">Keterangan</label>
              <input id="update-title" name="title" type="text" class="form-control" value="{{ old('title') }}">
            </fieldset>

            <?php $hasError = ($errors->has('permission') && old('id')) ?>
            <fieldset class="form-group {{ $hasError?'has-error':'' }}">
              <label for="update-permission">Permission</label>
              <textarea name="permission" id="update-permission" class="form-control">{{ old('permission') }}</textarea>
              @if ($hasError)
                @foreach($errors->get('permission') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </fieldset>
          </div>
          <div class="modal-footer">
            <button class="btn btn-info pull-left" type="submit">Update</button>
            <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
          </div>
        </div>
      </form>
    </div>
  @endif

@endsection

@section('script')
  @if ($isWritable)
    <script>
      oldId = "{{ $errors->isNotEmpty() ? old('id') : '' }}";
      $updateModal = $('#updateModal');

      inputs = {
        id: $('#update-id'),
        title: $('#update-title'),
        permission: $('#update-permission')
      };

      if (oldId) {
        $updateModal.modal();
      }

      $('.permission-list').on('click', 'a', event => {
        event.preventDefault();

        const $el = $(event.currentTarget);
        const data = $el.data();

        $.each(inputs, (key, $el) => {
          $el.val(data[key]);
        });

        $updateModal.modal();
      });

      $('form').submit(function() {
        $(this).addClass('form-loading form-loading-inverted');
      });
    </script>
  @endif
@endsection
