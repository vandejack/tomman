<!-- mark these classes so purifycss always include them -->
<div class="
  px-breakpoint-lg px-breakpoint-md px-breakpoint-sm px-breakpoint-xs

  alert-warning alert-danger alert-success alert-info alert-default

  open in px-open active disabled modal-backdrop pagination hidden-xs clearfix
  navbar-collapsing px-nav-expand px-nav-scrollable-area
  form-loading form-loading-inverted

  select2 select2-container select2-container--default select2-container--below select2-container--focus
  selection select2-selection select2-selection--single select2-selection__rendered select2-selection__arrow
  select2-selection__placeholder select2-container--open select2-dropdown select2-dropdown--below
  select2-search select2-search--dropdown select2-search__field select2-results select2-resulsts__options
  select2-results__option select2-results__option--highlighted select2-hidden-accessible select2-close-mask
  select2-selection__clear select2-search--inline select2-search--hide

  sortable-chosen sortable-ghost sortable-drag sortable-fallback

  fa-users fa-sitemap
"></div>
