<?php $hasError = $errors->has($field) ?>
<fieldset class="form-group form-message-light {{ $hasError?'has-error':'' }}">
  @isset($label)
    <label for="input-{{ $field }}">{!! $label !!}</label>
  @endisset

  @if ($canEdit)
    <select id="input-{{ $field }}" name="{{ $field }}" class="custom-select form-control"
      @isset($attributes)
        @foreach($attributes as $key => $val)
          @if ($val === true)
            {{ $key }}
          @else
            {{ $key }}="{{ $val }}"
          @endif
        @endforeach
      @endisset
    >
      {{ $slot }}
    </select>
  @else
    {{ $readonly }}
  @endif

  @if ($hasError)
    @foreach($errors->get($field) as $errorText)
      <small class="form-message light">{{ $errorText }}</small>
    @endforeach
  @endif
</fieldset>
