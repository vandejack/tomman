@component(
  'partial.form.select._tpl',
  ['field' => $field, 'label' => ($label ?? null), 'object' => $object, 'canEdit' => $canEdit, 'attributes' => ($attributes ?? null)]
)
  @foreach($options as $val)
    <?php $selected = (old($field) ?: ($object->$field ?? '')) == $val ? 'selected' : '' ?>
    <option value="{{ $val }}" {{ $selected }}>{{ $val }}</option>
  @endforeach

  @slot('readonly')
    <?php $value = $object->$field ?? reset($options) ?>
    <input type="hidden" name="{{ $field }}" value="{{ $value }}">
    <p class="form-control-static">{{ $value }}</p>
  @endslot
@endcomponent
