<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdcAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odc_audit(
              id BIGSERIAL PRIMARY KEY,
              odc_id BIGINT REFERENCES mcore.odc(id),
              user_id INTEGER REFERENCES auth.user(id),
              operation TEXT NOT NULL CHECK (operation <> ''),
              timestamp TIMESTAMP WITH TIME ZONE, 
              data JSON NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odc_audit(odc_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odc_audit');
    }
}
