<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcorePelangganAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.pelanggan_audit(
              id BIGSERIAL PRIMARY KEY,
              pelanggan_id BIGINT REFERENCES mcore.pelanggan(id),
              user_id INTEGER REFERENCES auth.user(id), 
              operation TEXT NOT NULL CHECK (operation <> ''),
              timestamp TIMESTAMP WITH TIME ZONE, 
              data JSON NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.pelanggan_audit(pelanggan_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.pelanggan_audit');
    }
}
