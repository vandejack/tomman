<?php

use Illuminate\Database\Seeder;
use App\Service\Auth\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(
            'Ambassador',
            '
                *: WRITE, READ_AUDIT
                auth.user: read
            '
        );

        Permission::create('DDO', 'mcore.*: write');

        Permission::create(
            'Teknisi Wasaka',
            '
                wasaka.gembok: read
                wasaka.work: write
            '
        );
    }
}
