<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            WorkzoneSeeder::class,
            PermissionSeeder::class,
            UserSeeder::class,

            McoreStoSeeder::class,
            McoreOdcSeeder::class,
            McoreOdpSeeder::class,
            McorePelangganSeeder::class
        ]);
    }
}
