<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Odc;

class McoreOdcSeeder extends Seeder
{
    public function run()
    {
        $wz = DB::table('auth.workzone')
            ->orderBy('id', 'DESC')
            ->select('id')
            ->limit(1)
            ->first()
        ;

        try {
            Odc::create(
                1,
                $wz->id,
                'ODC-SEED-001',
                4,
                12,
                0,
                0,
                0
            );

            Odc::create(
                1,
                $wz->id,
                'ODC-SEED-002',
                4,
                12,
                0,
                0,
                0
            );
        } catch(\Throwable $e) { throw $e; }
    }
}
