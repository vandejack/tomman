#!/bin/bash

DIR=/home/vagrant/code/provision

bash $DIR/install-tilde.sh

bash $DIR/configure-php.sh

bash $DIR/configure-redis.sh

bash $DIR/remove-mysql.sh

bash $DIR/install-postgis.sh

bash $DIR/init-app.sh
bash $DIR/init-db.sh

bash $DIR/configure-supervisor.sh

### Cleanup
sudo apt-get --yes autoremove
sudo apt-get --yes autoclean
###
