<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitDistribusi;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\Helper as Mcore;
use App\Service\Mcore\Map;
use App\Service\Mcore\Alpro;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdpCached;
use App\Service\Mcore\DistribusiCached;
use App\Service\Mcore\Link\OdcToOdpCached;

class DistribusiController extends Controller
{
    const KEY_RETURN_URL = 'returnUrl';

    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission(
            'mcore.distribusi',
            Authorization::WRITE
        );
        [$workzoneTree, $wz_mtime] = DistribusiCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/distribusi/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.distribusi.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$distribusiList, $dist_mtime] = DistribusiCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission(
            'mcore.distribusi',
            Authorization::WRITE
        );

        return view('mcore.distribusi.list', compact('workzoneData', 'distribusiList', 'canCreateNew'));
    }

    public function createForm(Request $request)
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);

        $distribusiData = null;
        $ret = $request->query('ret');
        if ($ret) {
            $request->session()->put(self::KEY_RETURN_URL, $ret);

            $token = explode('/', $ret);
            $refId = $token[5];
            $refData = null;

            if ($token[4] == 'odc') {
                $refData = OdcCached::getById($refId)[0];
            } elseif ($token[4] == 'odp') {
                $refData = OdpCached::getById($refId)[0];
            }

            if ($refData) {
                $distribusiData = (object)[
                    'workzone_id' => $refData->workzone_id,
                    'workzone_label' => $refData->workzone_label
                ];
            }
        }

        $canEdit = true;
        $capacities = DistribusiCached::CAPACITIES;

        return view(
            'mcore.distribusi.form',
            compact('workzoneTree', 'distribusiData', 'canEdit', 'capacities')
        );
    }

    public function create(SubmitDistribusi $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = DistribusiCached::create(
                $user->id,
                $request->workzone_id,
                $request->label,
                $request->capacity,
                $request->tubecount
            );
            $link = '<a href="/mcore/distribusi/'.$id.'">'.$request->label.'</a>';

            $session = $request->session();
            $response = $session->has(self::KEY_RETURN_URL)
                ? redirect($session->pull(self::KEY_RETURN_URL))
                : back()
            ;

            return $response->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data Kabel Distribusi '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function show($id)
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);

        [$distribusiData, $dist_mtime] = DistribusiCached::getById($id);

        [$linkList, $links_mtime] = OdcToOdpCached::getByDistribusi($id);
        $coreList = DistribusiCached::linksToCores($distribusiData->capacity, $linkList->all());

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.distribusi', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $distribusiData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        $capacities = DistribusiCached::CAPACITIES;

        return view(
            'mcore.distribusi.form',
            compact('workzoneTree', 'distribusiData', 'canEdit', 'capacities', 'coreList')
        );
    }

    public function update(SubmitDistribusi $request, $id)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            DistribusiCached::update(
                $user->id,
                $id,
                $request->workzone_id,
                $request->label,
                $request->capacity
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Kabel Distribusi'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public static function showRoute($id)
    {
        [$entityData, $dist_mtime] = DistribusiCached::getById($id);
        if (!$dist_mtime) {
            abort(404);
        }

        $entityData->type = 'distribusi';
        $entityData->shortLabel = 'DIST';

        $routeData = DistribusiCached::getRoutePath($id);
        if ($routeData) {
            $routeData = Mcore::refreshRouteDataPoint($routeData);
        } else {
            $routeData = [];
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.distribusi', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $entityData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.route.form', compact('entityData', 'canEdit', 'routeData'));
    }

    public static function submitRoute(Request $request, $id)
    {
        [$distribusiData, $dist_mtime] = DistribusiCached::getById($id);
        if (!$dist_mtime) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            DistribusiCached::saveRoutePath($user->id, $distribusiData->id, $request->route_path);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Jalur Kabel Feeder'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
