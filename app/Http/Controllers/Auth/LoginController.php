<?php

namespace App\Http\Controllers\Auth;

//use App\Jobs\GetHrmistaData;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Controller;
use App\Service\SSO;
//use App\Service\PortalTA\SSO as PortalTaSso;
use App\Service\SessionHelper;
use App\Service\Auth\User;
use App\Service\Auth\CredentialCache;

class LoginController extends Controller
{
    public function loginPage()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $nik  = $request->input('nik');
        $pass = $request->input('pass');
        $rememberUser = $request->input('remember');

        $localUser = User::getByLocalCredential($nik, $pass);

        if ($localUser) {
            if ($localUser->is_login_enabled) {
                SessionHelper::setCurrentUser($localUser);

                if ($rememberUser) {
                    $this->ensureLocalUserHasRememberToken($localUser);
                }
            } else {
                SessionHelper::flashCurrentUser($localUser);
            }

            return $this->successResponse($localUser->remember_token);
        } else {
            $ssoResult = SSO::checkCredential($nik, $pass);
            if ($ssoResult->success) {
                CredentialCache::store($nik, $pass);
                $authSession = $this->generateLocalUser($nik, $ssoResult, $rememberUser);

                // TODO: user data via auth table and hrmista, preferrably via queue
                // TODO: need to decide what kind of data to take from HRMISTA
                //GetHrmistaData::dispatch($nik);

                if ($authSession->type != User::TYPE_NEWUSER && $authSession->is_login_enabled) {
                    SessionHelper::setCurrentUser($authSession);
                } else {
                    SessionHelper::flashCurrentUser($authSession);
                }

                return $this->successResponse($authSession->remember_token);
            } else {
                return $this->failureResponse($request, $ssoResult);
            }
        }
    }

    public function logout()
    {
        SessionHelper::flush();
        $cookie = Cookie::forget(SessionHelper::KEY_PERSISTENT_COOKIE);
        return redirect('/login')->withCookie($cookie);
    }

    private function generateLocalUser($nik, $ssoResult, $rememberUser)
    {
        $localUser = User::getByLogin($nik);
        if (!$localUser) {
            //$ssoCookie = serialize(PortalTaSso::getCookie($nik, $pass));
            $data = [
                'login'          => $nik,
                'nama'           => $ssoResult->nama,
                //'sso_cookie'     => $ssoCookie,
                'remember_token' => $this->generateRememberToken($nik)
            ];

            $insertId = User::create($data);
            $localUser = User::getById($insertId);
        } elseif ($rememberUser) {
            $this->ensureLocalUserHasRememberToken($localUser);
        }

        return $localUser;
    }

    private function ensureLocalUserHasRememberToken($localUser)
    {
        $token = $localUser->remember_token;

        if (!$token) {
            $token = $this->generateRememberToken($localUser->login);
            User::update($localUser->id, [
                'remember_token' => $token
            ]);
            $localUser->remember_token = $token;
        }

        return $token;
    }

    private function generateRememberToken($nik)
    {
        return md5($nik . microtime());
    }

    private function successResponse($rememberToken)
    {
        $currentUser = SessionHelper::getCurrentUser();
        $isLoginEnabled = $currentUser->is_login_enabled;

        if (SessionHelper::hasLoginRedirect() && $isLoginEnabled) {
            $url = SessionHelper::pullLoginRedirect();
        } else {
            $url = '/';
        }

        $response = redirect($url);
        if ($rememberToken) {
            $expire = 30 * 24 * 60; // one month
            $response->cookie(SessionHelper::KEY_PERSISTENT_COOKIE, $rememberToken, $expire, '', '', true, true);
        }

        return $response;
    }

    private function failureResponse(Request $request, $ssoResult): RedirectResponse
    {
        // flash old input
        $request->flash();

        switch ($ssoResult->error) {
            case SSO::ERR_PASSWORD_WRONG:
                $alertText = '<strong>Password</strong> salah. User akan dikunci jika 3x salah password';
                break;

            case SSO::ERR_USER_NOT_EXIST:
                $alertText = '<strong>NIK</strong> tidak terdaftar';
                break;

            case SSO::ERR_HTTP_EXCEPTION:
                $alertText = 'GAGAL MENGHUBUNGI SERVER SSO';
                break;

            default:
                $alertText = 'LOGIN GAGAL';
        }

        return back()->with('alerts', [
            [
                'type' => 'danger',
                'text' => $alertText
            ]
        ]);
    }
}
