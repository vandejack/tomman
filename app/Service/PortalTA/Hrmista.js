"use strict";

const puppeteer = require('puppeteer');

const baseUrl = 'https://apps.telkomakses.co.id/portal';

const username = process.argv[1] || process.env.LOGIN;
const password = process.argv[2] || process.env.PASSWORD;

if (!username || !password) {
  const result = {
    error: {
      code: -1,
      text: 'invalid parameter'
    }
  };
  console.log(JSON.stringify(result));
  process.exit(-1);
}

const getData = () => {
  const dom = document.querySelector('iframe').contentDocument;

  const result = {
    photoUrl: dom.querySelector('.img-box').src
  };

  return result;
};

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  try {
    // login
    await page.goto(baseUrl + '/login.php');
    await page.type('#username', username);
    await page.type('#password', password);
    await page.click('[name="submit"]');
    await page.waitForNavigation();
    // TODO: login failure

    // HRMISTA / WIMATA
    await page.goto(baseUrl + '/hr.php?url=wimata');

    const result = await page.evaluate(getData);
    console.log(JSON.stringify(result));
  }
  catch(e) {
    const result = {
      error: {
        code: 101,
        text: 'Unhandled Exception',
        exception: e
      }
    };
    console.log(JSON.stringify(result));
    process.exit(101);
  }

  await browser.close();
})();
