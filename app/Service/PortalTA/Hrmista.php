<?php

namespace App\Service\PortalTA;

class Hrmista
{
    /**
     * @param $login
     * @param $password
     *
     * @return object|bool data object or FALSE on failure
     * {
     *   photoUrl
     * }
     */
    public static function retrieve($login, $password)
    {
        $result = execWithEnv('node Hrmista.js', ['LOGIN' => $login, 'PASSWORD' => $password]);
        $hrData = json_decode($result);
        if (isset($hrData->error)) {
            return false;
        }

        return $hrData;
    }
}
