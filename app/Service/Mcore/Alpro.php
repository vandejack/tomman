<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;

class Alpro
{
    public const TYPES = [
        0 => 'Tiang',
        1 => 'Handhole',
        2 => 'Manhole',
        3 => 'Closure',
        4 => 'Slack'
    ];

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param int $type
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function create(int $user_id, int $workzone_id, int $type, float $lat, float $lng)
    {
        DB::transaction(function () use ($user_id, $workzone_id, $type, $lat, $lng) {
            $data = compact('workzone_id', 'type');
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");
            $id = DB::table('mcore.alpro')->insertGetId($data);

            $data['coordinate'] = compact('lat', 'lng');
            $auditData = [
                'alpro_id' => $id,
                'user_id' => $user_id,
                'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
                'operation' => 'insert',
                'data' => json_encode($data)
            ];
            DB::table('mcore.alpro_audit')->insert($auditData);
        });
    }
}
