<?php

namespace App\Service\Mcore\Link;

use App\Service\Cache;
use App\Service\Mcore\OdcCached;

class OdcSplitterCached
{
    /**
     * @param $userId
     * @param $odcId
     * @param $odcPanel
     * @param $odcPort
     * @param $splitterId
     * @param $splitterPort
     * @throws \Throwable
     */
    public static function plug($userId, $odcId, $odcPanel, $odcPort, $splitterId, $splitterPort)
    {
        OdcSplitter::plug($userId, $odcId, $odcPanel, $odcPort, $splitterId, $splitterPort);

        Cache::flushTags([OdcCached::tagById($odcId)]);
    }

    /**
     * @param $userId
     * @param $odcId
     * @param $odcPanel
     * @param $odcPort
     * @throws \Throwable
     */
    public static function unplugByOdc($userId, $odcId, $odcPanel, $odcPort)
    {
        OdcSplitter::unplugByOdc($userId, $odcId, $odcPanel, $odcPort);

        Cache::flushTags([OdcCached::tagById($odcId)]);
    }
}
