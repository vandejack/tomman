<?php

namespace App\Service\Auth;

use App\Service\Cache;

class WorkzoneCached
{
    public const PATH_DELIMETER = Workzone::PATH_DELIMETER;

    const PREFIX = 'Auth.Workzone:';
    const KEY_ALL = self::PREFIX.'All';
    const KEY_BYID = self::PREFIX.'Id=';
    const KEY_BYPATH = self::PREFIX.'Path=';

    public static function tagById($id)
    {
        return self::PREFIX.$id;
    }

    public static function tagsByWorkzoneId($workzone_id, callable $tagGenerator)
    {
        [$workzoneData, $mtime] = self::getById($workzone_id);
        $tags = self::tagsByWorkzonePath($workzoneData->path, $tagGenerator);

        return $tags;
    }

    public static function tagsByWorkzonePath($path, callable $tagGenerator)
    {
        $workzoneIdList = explode(WorkzoneCached::PATH_DELIMETER, $path);
        $tags = array_map($tagGenerator, $workzoneIdList);

        return $tags;
    }

    public static function flushTagByWorkzoneId($id, callable $tagGenerator)
    {
        $tags = self::tagsByWorkzoneId($id, $tagGenerator);
        Cache::flushTags($tags);
    }

    public static function flushIfKeyExists(
        $key,
        $workzoneId,
        $workzoneIdField,
        $workzonePathField,
        callable $tagGenerator,
        array $additionalTags = []
    ) {
        [$data, $mtime] = Cache::get($key);
        if ($mtime !== null) {
            $tags = self::tagsByWorkzoneId($workzoneId, $tagGenerator);
            $tags = array_merge($tags, $additionalTags);

            if ($workzoneId != $data->$workzoneIdField) {
                $oldTags = self::tagsByWorkzonePath($data->$workzonePathField, $tagGenerator);
                $tags = array_merge($tags, $oldTags);
            }

            Cache::flushTags($tags);
        }
    }

    public static function idByPath($path)
    {
        $pos = strrpos($path, self::PATH_DELIMETER);
        $id = ($pos === false) ? $path : substr($path, $pos + 1);

        return $id;
    }

    public static function all()
    {
        $stored = Cache::get(self::KEY_ALL);

        if ($stored[0] === null) {
            $data = Workzone::all();
            $stored = Cache::set(self::KEY_ALL, $data);
        }

        return $stored;
    }

    public static function getById($id)
    {
        $key = self::KEY_BYID.$id;
        $stored = Cache::get($key);

        if ($stored[0] === null) {
            $data = Workzone::getById($id);
            $stored = Cache::set($key, $data);
        }

        return $stored;
    }

    public static function getByPath($path)
    {
        $key = self::KEY_BYPATH.$path;
        $stored = Cache::get($key);

        if ($stored[0] === null) {
            $data = Workzone::getByPath($path);
            $stored = Cache::set($key, $data);
        }

        return $stored;
    }

    /**
     * @param string $label
     * @param string|null $path
     * @return object newly-created object, retrieved from db
     * @throws \Throwable
     */
    public static function create(string $label, string $path = null)
    {
        // all cache are based on Workzone
        // workzone should be set at initial setup and rarely altered
        // creating workzone will flush entire cache
        Cache::flushAll();

        return Workzone::create($label, $path);
    }

    public static function update($id, $label)
    {
        // all cache are based on Workzone
        // workzone should be set at initial setup and rarely altered
        // updating workzone will flush entire cache
        Cache::flushAll();

        return Workzone::update($id, $label);
    }
}
