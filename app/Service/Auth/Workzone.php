<?php

namespace App\Service\Auth;

use Illuminate\Support\Facades\DB;

class Workzone
{
    public const PATH_DELIMETER = '.';

    private static function db()
    {
        return DB::table('auth.workzone');
    }

    public static function all()
    {
        $all = self::db()->orderBy('path')->get();
        $result = self::grow($all);

        return $result;
    }

    public static function getById($id)
    {
        return self::db()->where('id', $id)->first();
    }

    public static function getFlatTrunkById($id)
    {
        $data = self::getById($id);
        return self::getFlatTrunkByPath($data->path);
    }

    public static function getFlatTrunkByPath($path)
    {
        return self::db()->where('path', '<@', $path)->orderBy('path')->get();
    }

    public static function getByPath($path)
    {
        $list = self::getFlatTrunkByPath($path);
        $result = self::grow($list);

        return $result;
    }

    /**
     * @param string $label
     * @param string $path
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     * @throws \Throwable
     */
    public static function create(string $label, string $path = '1')
    {
        $id = null;
        DB::transaction(function () use (&$id, $label, $path) {
            $id = self::db()->insertGetId(compact('label'));

            $path = !empty($path) ? $path.self::PATH_DELIMETER.$id : $id;
            self::db()
                ->where('id', $id)
                ->update(compact('path'));
        });

        $row = self::db()
            ->where('id', $id)
            ->first();

        return $row;
    }

    public static function update($id, $label)
    {
        self::db()->where('id', $id)->update(['label' => $label]);

        $row = self::db()
            ->where('id', $id)
            ->first();

        return $row;
    }

    public static function grow($rows)
    {
        $result = [];
        $previousRows = [];

        foreach ($rows as $row) {
            $row->children = [];

            while (count($previousRows)) {
                $previous = array_pop($previousRows);
                $path = $previous->path.self::PATH_DELIMETER.$row->id;
                if ($path == $row->path) {
                    $previous->children[] = $row;
                    $previousRows[] = $previous;
                    $previousRows[] = $row;

                    continue 2;
                } else {
                    usort($previous->children, [__CLASS__, 'sortHandler']);
                }
            }

            $previousRows[] = $row;
            $result[] = $row;
        }

        usort($result, [__CLASS__, 'sortHandler']);
        return $result;
    }

    private static function sortHandler($a, $b)
    {
        return strnatcasecmp($a->label, $b->label);
    }
}
