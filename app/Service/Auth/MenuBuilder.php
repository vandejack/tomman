<?php

namespace App\Service\Auth;

class MenuBuilder
{
    public static function build($user)
    {
        $menu = [];

        $authMenu = self::buildAuth($user);
        if (count($authMenu)) {
            $menu[] = (object)[
                'class' => 'fas fa-users',
                'label' => 'Auth',
                'submenu' => $authMenu
            ];
        }

        $mcoreMenu = self::buildMcore($user);
        if (count($mcoreMenu)) {
            $menu[] = (object)[
                'class' => 'fas fa-sitemap',
                'label' => 'MCore',
                'submenu' => $mcoreMenu
            ];
        }

        return $menu;
    }

    private static function buildAuth($user)
    {
        $menu = [];

        if (self::canRead('auth.workzone', $user)) {
            $menu[] = (object)[
                'label' => 'Workzone',
                'url'   => '/auth/workzone'
            ];
        }

        if (self::canRead('auth.permission', $user)) {
            $menu[] = (object)[
                'label' => 'Permission',
                'url'   => '/auth/permission'
            ];
        }

        if (self::canRead('auth.user', $user)) {
            $menu[] = (object)[
                'label' => 'User',
                'url'   => '/auth/user'
            ];
        }

        return $menu;
    }

    private static function buildMcore($user)
    {
        $menu = [];

        if (self::canRead('mcore.sto', $user)) {
            $menu[] = (object)[
                'label' => 'STO',
                'url' => '/mcore/sto'
            ];
        }

        if (count($menu)) {
            $mapMenu = (object)[
                'label' => 'Map',
                'url' => '/mcore'
            ];
            array_unshift($menu, $mapMenu);
        }

        if (self::canRead('mcore.odc', $user)) {
            $menu[] = (object)[
                'label' => 'ODC',
                'url' => '/mcore/odc'
            ];
        }

        if (self::canRead('mcore.odp', $user)) {
            $menu[] = (object)[
                'label' => 'ODP',
                'url' => '/mcore/odp'
            ];
        }

        if (self::canRead('mcore.pelanggan', $user)) {
            $menu[] = (object)[
                'label' => 'Pelanggan',
                'url' => '/mcore/pelanggan'
            ];
        }

        if (self::canRead('mcore.feeder', $user)) {
            $menu[] = (object)[
                'label' => 'Feeder',
                'url' => '/mcore/feeder'
            ];
        }

        if (self::canRead('mcore.distribusi', $user)) {
            $menu[] = (object)[
                'label' => 'Distribusi',
                'url' => '/mcore/distribusi'
            ];
        }

        if (self::canWrite('mcore.alpro', $user)) {
            $menu[] = (object)[
                'label' => 'Input Alpro',
                'url' => '/mcore/alpro'
            ];
        }

        return $menu;
    }

    private static function canRead($module, $user)
    {
        return Authorization::hasPermission($module, Authorization::READ, $user->permission);
    }

    private static function canWrite($module, $user)
    {
        return Authorization::hasPermission($module, Authorization::WRITE, $user->permission);
    }
}
