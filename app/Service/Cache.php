<?php

namespace App\Service;

use Illuminate\Support\Facades\Redis;

class Cache
{
    const TAG_PREFIX = 'tag:';
    const TIMESTAMP_SUFFIX = ':ts';

    public static function store(string $key, callable $dataSource, callable $tagGenerator = null, $ttl = 0)
    {
        $stored = self::get($key);
        if ($stored[1] === null) {
            $data = $dataSource();
            $tags = isset($tagGenerator) ? $tagGenerator() : [];
            $stored = self::set($key, $data, $tags, $ttl);
        }

        return $stored;
    }

    public static function get(string $key)
    {
        $stored = Redis::mget([$key, $key.self::TIMESTAMP_SUFFIX]);
        if ($stored[0] !== null) {
            $stored[0] = unserialize($stored[0]);
        }

        return $stored;
    }

    public static function set(string $key, $value, array $tags = [], $ttl = 0)
    {
        $ts = time();
        Redis::pipeline(function ($redis) use ($key, $value, $ts, $tags, $ttl) {
            $redis->set($key, serialize($value));
            $redis->set($key.self::TIMESTAMP_SUFFIX, $ts);

            if ($ttl > 0) {
                $redis->expire($key, $ttl);
            }

            foreach ($tags as $tag) {
                $redis->rpush(self::TAG_PREFIX.$tag, $key);
            }
        });

        return [$value, $ts];
    }

    public static function flushTags(array $tags)
    {
        $tagKeys = array_map(
            function ($tag) {
                return self::TAG_PREFIX.$tag;
            },
            $tags
        );
        $allKeys = Redis::pipeline(function ($redis) use ($tagKeys) {
            foreach ($tagKeys as $tagKey) {
                $redis->lrange($tagKey, 0, -1);
            }
        });

        Redis::pipeline(function ($redis) use ($allKeys, $tagKeys) {
            foreach ($allKeys as $keys) {
                $unsetKeys = $keys;
                foreach ($keys as $key) {
                    $unsetKeys[] = $key.self::TIMESTAMP_SUFFIX;
                }

                $redis->unlink($unsetKeys);
            }
            $redis->unlink($tagKeys);
        });
    }

    public static function flushKeys(array $keys)
    {
        $tsKeys = array_map(
            function ($key) {
                return $key.self::TIMESTAMP_SUFFIX;
            },
            $keys
        );

        $keys = array_merge($keys, $tsKeys);
        Redis::unlink($keys);
    }

    public static function del(string $key)
    {
        Redis::unlink([$key, $key.self::TIMESTAMP_SUFFIX]);
    }

    public static function flushAll()
    {
        Redis::flushdb('async');
    }
}
